package models;

public class Person {
    private String FirstName;
    private String LastName;
    private String Gender;
    private String Contact;
    private String Email;
    
    public Person() {
    }

    public Person(String firstName, String lastName, String gender, String contact, String email) {
        FirstName = firstName;
        LastName = lastName;
        Gender = gender;
        Contact = contact;
        Email = email;
    }

    public String getFirstName() {
        
        return this.FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }

    public String getContact() {
        return Contact;
    }

    public void setContact(String contact) {
        Contact = contact;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String toString() {
        return "Name: " + this.FirstName + " " + this.LastName;
    }
}
