import models.Person;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");

        Person person1 = new Person();
        person1.setFirstName("Hoang");
        person1.setLastName("Le");
        person1.setGender("male");
        person1.setEmail("le.hoang@gmail.com");
        
        System.out.println("Person 1: " + person1.toString());

        Person person2 = new Person("XA", "Le","male","092123456","xuan.le@gmail.com");

        System.out.println("Person 2: " + person2.toString());
    }
}
